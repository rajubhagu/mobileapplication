package com.example.myapplication;


import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.ui.fragments.downloads.DownloadsViewModel;
import com.example.myapplication.ui.fragments.home.HomeFragment;
import com.example.myapplication.ui.fragments.home.HomeFragmentViewModel;
import com.example.myapplication.ui.fragments.search.SearchViewModel;
import com.example.myapplication.ui.fragments.upcoming.UpcomingViewModel;
import com.example.myapplication.ui.login.LoginViewModel;
import com.example.myapplication.ui.main.MainViewModel;
import com.example.myapplication.ui.signup.SignUpViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ViewModelProviderFactory extends ViewModelProvider.NewInstanceFactory {

    private DataManager dataManager;
    private SchedulerProvider schedulerProvider;


    @Inject
    public ViewModelProviderFactory(DataManager dataManager, SchedulerProvider schedulerProvider) {
        this.dataManager = dataManager;
        this.schedulerProvider = schedulerProvider;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if(modelClass.isAssignableFrom(LoginViewModel.class)){
            return (T) new LoginViewModel(dataManager,schedulerProvider);
        }
        else if(modelClass.isAssignableFrom(SignUpViewModel.class)){
            return (T) new SignUpViewModel(dataManager,schedulerProvider);
        }
        else if(modelClass.isAssignableFrom(MainViewModel.class)){
            return (T) new MainViewModel(dataManager,schedulerProvider);
        }
        else if(modelClass.isAssignableFrom(HomeFragmentViewModel.class)){
            return (T) new HomeFragmentViewModel(dataManager,schedulerProvider);
        }
        else if(modelClass.isAssignableFrom(SearchViewModel.class)){
            return (T) new SearchViewModel(dataManager,schedulerProvider);
        }
        else if(modelClass.isAssignableFrom(UpcomingViewModel.class)){
            return (T) new UpcomingViewModel(dataManager,schedulerProvider);
        }

       throw  new IllegalArgumentException("Unknown Class"+modelClass.getName());
    }
}
