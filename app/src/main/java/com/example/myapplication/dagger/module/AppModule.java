package com.example.myapplication.dagger.module;

import android.app.Application;
import android.content.Context;

import com.example.myapplication.data.AppDataManager;
import com.example.myapplication.data.DataManager;
import com.example.myapplication.data.network.ApiHelper;
import com.example.myapplication.data.network.AppApiHelper;
import com.example.myapplication.data.pref.PreferenceHelper;
import com.example.myapplication.data.pref.SchedulePreferenceHelper;
import com.example.myapplication.ui.fragments.home.HomeFragmentViewModel;
import com.example.myapplication.ui.fragments.home.adapter.CustomPagersAdapter;
import com.example.myapplication.ui.fragments.home.adapter.VideoListsAdapter;
import com.example.myapplication.ui.fragments.home.adapter.VideoListsByTagAdapter;
import com.example.myapplication.ui.fragments.search.SearchViewModel;
import com.example.myapplication.ui.fragments.search.adapter.SearchVideoAdapter;


import com.example.myapplication.ui.fragments.upcoming.UpcomingViewModel;
import com.example.myapplication.ui.fragments.upcoming.adapter.UpComingVideoAdapter;
import com.example.myapplication.utils.rx.AppSchedulerProvider;
import com.example.myapplication.utils.rx.SchedulerProvider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {


    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }


    @Provides
    ApiHelper provideApiHelper(){
       return new AppApiHelper();
    }
//
//    @Provides
//    VideoListAdapter provideVideoListAdapter(HomeViewModel homeViewModel){
//        return  new VideoListAdapter(homeViewModel);
//    }
//
//
//
//    @Provides
//    VideoListByTagAdapter provideVideoListByTag(HomeViewModel homeViewModel, String tag){
//        return new VideoListByTagAdapter(homeViewModel,tag);
//    }
//
//    @Provides
//    CustomPagerAdapter provideCustomPagerAdapter(HomeViewModel homeViewModel){
//        return  new CustomPagerAdapter(homeViewModel);
//    }



    @Provides
    PreferenceHelper providePreferenceHelper(){
        return  new SchedulePreferenceHelper();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }


    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }



    @Provides
    VideoListsAdapter provideVideoListsAdapter(HomeFragmentViewModel homeViewModel){
        return  new VideoListsAdapter(homeViewModel);
    }



    @Provides
    VideoListsByTagAdapter provideVideoListsByTag(HomeFragmentViewModel homeViewModel, String tag){
        return new VideoListsByTagAdapter(homeViewModel,tag);
    }

    @Provides
    CustomPagersAdapter provideCustomPagersAdapter(HomeFragmentViewModel homeViewModel){
        return  new CustomPagersAdapter(homeViewModel);
    }



    @Provides
    SearchVideoAdapter provideSearchVideoAdapter(SearchViewModel searchViewModel){
        return  new SearchVideoAdapter(searchViewModel);
    }

    @Provides
    UpComingVideoAdapter provideUpcomingVideoAdapter(UpcomingViewModel upcomingViewModel){
        return  new UpComingVideoAdapter(upcomingViewModel);
    }


}
