package com.example.myapplication.dagger.builder;

import com.example.myapplication.ui.fragments.home.HomeFragmentProvider;
import com.example.myapplication.ui.fragments.search.SearchFragment;
import com.example.myapplication.ui.fragments.upcoming.UpcomingFragment;
import com.example.myapplication.ui.login.LoginActivity;
import com.example.myapplication.ui.main.MainActivity;
import com.example.myapplication.ui.signup.SignUpActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();


    @ContributesAndroidInjector
    abstract SignUpActivity bindSignUpActivity();


    @ContributesAndroidInjector
    abstract SearchFragment bindSearchFragment();

    @ContributesAndroidInjector
    abstract UpcomingFragment bindUpcomingFragment();

    @ContributesAndroidInjector(modules = {HomeFragmentProvider.class})
    abstract MainActivity bindMainActivity();
}
