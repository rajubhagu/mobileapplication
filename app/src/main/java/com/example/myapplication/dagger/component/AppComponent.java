package com.example.myapplication.dagger.component;

import android.app.Application;

import com.example.myapplication.BaseApplication;
import com.example.myapplication.dagger.builder.ActivityBuilder;
import com.example.myapplication.dagger.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;


@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilder.class})
public interface AppComponent {
    void  inject(BaseApplication application);

    @Component.Builder
    interface  Builder{

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
