package com.example.myapplication.ui.fragments.home.adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.databinding.VideoItemBinding;
import com.example.myapplication.ui.fragments.home.HomeFragmentViewModel;

import javax.inject.Inject;

public class VideoListsByTagAdapter extends  RecyclerView.Adapter<VideoListsByTagAdapter.ItemViewHolder> {

    HomeFragmentViewModel homeViewModel;
    String tag;


    @Inject
    public VideoListsByTagAdapter(HomeFragmentViewModel homeViewModel, String tag) {
        this.homeViewModel=homeViewModel;
//        this.VideoList=list;
        this.tag=tag;

    }

    @NonNull
    @Override
    public VideoListsByTagAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoItemBinding binding=  DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.video_item,parent,false);
        return new VideoListsByTagAdapter.ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(VideoListsByTagAdapter.ItemViewHolder holder, int position) {

        if(position == 0 ){

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(50, 25, 35, 25);

            holder.binding.llRoot.setOrientation(LinearLayout.VERTICAL);
            holder.binding.llRoot.setLayoutParams(params);
        }
        else {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 25, 35, 25);
            holder.binding.llRoot.setOrientation(LinearLayout.VERTICAL);
            holder.binding.llRoot.setLayoutParams(params);
        }

        //homeViewModel.getImage()
        (holder).binding.ivVideo.setImageBitmap(homeViewModel.getImageByTag(position,holder.binding.ivVideo,tag));

        (holder).binding.tvTitle.setText(tag);

    }

    @Override
    public int getItemCount() {
        return homeViewModel.getVideoCountByTag(tag);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        VideoItemBinding binding;
        public ItemViewHolder(VideoItemBinding videoItemBinding) {
            super(videoItemBinding.getRoot());
            this.binding=videoItemBinding;
        }
    }
}
