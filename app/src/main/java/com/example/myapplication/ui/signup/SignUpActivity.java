package com.example.myapplication.ui.signup;

import android.content.Intent;

import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;
import com.example.myapplication.ViewModelProviderFactory;
import com.example.myapplication.databinding.SignUpActivityBinding;
import com.example.myapplication.ui.Base.BaseActivity;
import com.example.myapplication.ui.login.LoginActivity;

import javax.inject.Inject;

public class SignUpActivity extends BaseActivity<SignUpActivityBinding, SignUpViewModel> implements  SignUpNavigator  {

    @Inject
    ViewModelProviderFactory factory;
    private SignUpViewModel signUpViewmodel;

    public static Intent newIntent(LoginActivity loginActivity) {
        Intent intent = new Intent(loginActivity.getApplicationContext(), SignUpActivity.class);
        return intent;
    }


    @Override
    public SignUpViewModel getViewModel() {
       signUpViewmodel= ViewModelProviders.of(this,factory).get(SignUpViewModel.class);
        return signUpViewmodel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.sign_up_activity;
    }

    @Override
    public int getBindingVariable() {
        return com.example.myapplication.BR.viewModel;
    }
}
