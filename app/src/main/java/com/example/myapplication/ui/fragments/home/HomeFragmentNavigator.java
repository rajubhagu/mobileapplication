package com.example.myapplication.ui.fragments.home;

import android.graphics.Bitmap;
import android.widget.ImageView;

public interface HomeFragmentNavigator {

    void handleError(String message);

    void setVideoList();

    void setImage(Bitmap image, ImageView ivVideo);

    void setBannerList();

    void setVideoListByTag(String tag);
}
