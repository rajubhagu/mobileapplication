package com.example.myapplication.ui.main;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;
import com.example.myapplication.ViewModelProviderFactory;
import com.example.myapplication.databinding.ActivityMainBinding;
import com.example.myapplication.ui.Base.BaseActivity;
import com.example.myapplication.ui.fragments.downloads.DownloadsFragment;
import com.example.myapplication.ui.fragments.home.HomeFragment;
import com.example.myapplication.ui.fragments.moreoptions.MoreOptionsFragment;
import com.example.myapplication.ui.fragments.search.SearchFragment;
import com.example.myapplication.ui.fragments.upcoming.UpcomingFragment;
import com.google.android.material.tabs.TabLayout;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseActivity<ActivityMainBinding,MainViewModel> implements MainNavigator, HasSupportFragmentInjector {


    @Inject
    ViewModelProviderFactory factory;


    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    MainViewModel mainViewModel;
    ActivityMainBinding binding;

    HomeFragment homeFragment;
    SearchFragment searchFragment;
    DownloadsFragment downloadsFragment;
    MoreOptionsFragment moreOptionsFragment;
    UpcomingFragment upcomingFragment;

    @Override
    public MainViewModel getViewModel() {
        mainViewModel= ViewModelProviders.of(this,factory).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public int getBindingVariable() {
       return com.example.myapplication.BR.viewModel;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        mainViewModel.setNavigator(this);

    }


    @Override
    public void setUp() {
        loadHomeFragment();
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                loadFragment(tab.getTag().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//        binding.home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadFragment("home");
//
//            }
//        });
//        binding.search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadFragment("search");
//
//            }
//        });
//        binding.download.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadFragment("downloads");
//
//            }
//        }); binding.comingSoon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadFragment("coming soon");
//
//            }
//        }); binding.more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadFragment("more");
//
//            }
//        });

    }

    private void loadFragment(String tag) {

        switch (tag){
            case "home":
                loadHomeFragment();
            case "downloads":
                loadDownloadsFragment();
            case "more":
                loadMoreFragment();
            case "search":
                loadSearchFragment();
            case "coming soon":
                loadUpcomingFragment();
                default:

        }
    }

    private void loadUpcomingFragment() {
        if(upcomingFragment==null){

            upcomingFragment= new UpcomingFragment();
//            binding.frameLayout.addView();

        }
        replaceFragment(upcomingFragment);

    }

    private void loadSearchFragment() {
        if(searchFragment==null){

            searchFragment=new SearchFragment();

        }

        replaceFragment(searchFragment);
    }

    private void loadMoreFragment() {
        if(moreOptionsFragment==null){

            moreOptionsFragment=new MoreOptionsFragment();

        }

        replaceFragment(moreOptionsFragment);
    }

    private void loadDownloadsFragment() {
        if(downloadsFragment==null){

            downloadsFragment=new DownloadsFragment();

        }
        replaceFragment(downloadsFragment);
    }

    private void loadHomeFragment() {
        if(homeFragment==null){

            homeFragment=HomeFragment.newInstance();
//            homeFragment=new HomeFragment();
        }
        replaceFragment(homeFragment);
    }

    public void replaceFragment(Fragment fragment) {

        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment).commitAllowingStateLoss();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}
