package com.example.myapplication.ui.fragments.upcoming;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.data.network.model.Video;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpcomingViewModel extends BaseViewModel<UpcomingNavigator> {


    private List<Video> upComingVideos;

    @Inject
    public UpcomingViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    @Override
    public void setNavigator(UpcomingNavigator navigator) {
        super.setNavigator(navigator);
        getUpcomingVideos();
    }

    private void getUpcomingVideos() {

        Call<List<Video>> call = getmDataManager().getUpcomingVideos();
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {

                if(!response.body().isEmpty()){
                    upComingVideos =response.body();
                    getmNavigator().setUpUpcomingVideos();
                }
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {

                getmNavigator().handleError(t.getMessage());
            }
        });


    }

    public Bitmap setImageBitMap(ImageView imageView, int position) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                if (getVideosList().get(position).getImageUrl() != null) {

                    try {
                        URL url = new URL(getVideosList().get(position).getImageUrl());
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        getNavigator().setImage(image,imageView);
//                        ivVideo.setImageBitmap(image);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

        });
        thread.start();

        return null;
    }

    public List<Video> getVideosList() {

        if(upComingVideos.size()>0){
            return upComingVideos;
        }
        return null;
    }

    public int getVideosSize() {
        if(upComingVideos.size()>0){
            return upComingVideos.size();
        }
        return 0;
    }
}
