package com.example.myapplication.ui.fragments.home.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.databinding.VideoItemBinding;
import com.example.myapplication.ui.fragments.home.HomeFragmentViewModel;

import javax.inject.Inject;

public class VideoListsAdapter extends RecyclerView.Adapter<VideoListsAdapter.ItemViewHolder> {


    private HomeFragmentViewModel homeViewModel;

    @Inject
    public VideoListsAdapter(HomeFragmentViewModel model)
    {
        this.homeViewModel=model;
    }


    @Override
    public VideoListsAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        VideoItemBinding binding=  DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.video_item,parent,false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder( ItemViewHolder holder, int position) {

        (holder).binding.ivVideo.setImageBitmap(homeViewModel.getImage(position,holder.binding.ivVideo));

        (holder).binding.tvTitle.setText(homeViewModel.getVideosList().get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return homeViewModel.getVideosSize();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        VideoItemBinding binding;
        public ItemViewHolder(VideoItemBinding videoItemBinding) {
            super(videoItemBinding.getRoot());
            this.binding=videoItemBinding;
        }




    }
}
