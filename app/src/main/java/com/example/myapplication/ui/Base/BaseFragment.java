package com.example.myapplication.ui.Base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;

import static android.widget.Toast.LENGTH_SHORT;


public abstract class BaseFragment<T extends ViewDataBinding,V extends  BaseViewModel>   extends Fragment  {

    private ProgressDialog mProgressDialog;
    private T mViewDataBinding;
    private V mViewModel;
    private BaseActivity mActivity;

    private  View mRootView;


    public abstract V getViewModel();

    public abstract
    @LayoutRes
    int getLayoutId();

    public abstract int getBindingVariable();

    @Override
    public void onCreate( Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        mViewModel=getViewModel();
        performDataBinding();
    }

    public void performDependencyInjection() {

        AndroidSupportInjection.inject(this);
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }


    public void showLoading() {
        hideLoading();
        mProgressDialog= new  ProgressDialog(getActivity());
        mProgressDialog.show();
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(getActivity(), getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public  void  handleError(String s){
        Toast.makeText(getActivity(),s,LENGTH_SHORT);
    }


    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        mViewDataBinding=DataBindingUtil.inflate(inflater,getLayoutId(),container,false);
        mRootView=mViewDataBinding.getRoot();
        return  mRootView;
    }

    @Override
    public void onAttach( Context context) {
        super.onAttach(context);
        if(context instanceof  BaseActivity){
            BaseActivity activity= (BaseActivity) context;
            this.mActivity=activity;
            activity.onFragmentAttached();

        }
    }

    @Override
    public void onViewCreated( View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setVariable(getBindingVariable(),mViewModel);
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.executePendingBindings();
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public void setBaseActivity(BaseActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        mActivity=null;
        super.onDetach();
    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);

    }
}
