package com.example.myapplication.ui.main;

import com.example.myapplication.ui.Base.BaseNavigator;

public interface MainNavigator extends BaseNavigator {
    void setUp();
}
