package com.example.myapplication.ui.signup;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;

public class SignUpViewModel extends BaseViewModel<SignUpNavigator> {

    public SignUpViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
