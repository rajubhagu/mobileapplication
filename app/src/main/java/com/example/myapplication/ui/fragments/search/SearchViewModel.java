package com.example.myapplication.ui.fragments.search;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.widget.ImageView;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.data.network.model.Video;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchViewModel extends BaseViewModel<SearchNavigator> {

    List<Video> videoBySearchText;


    public SearchViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);


    }

    @Override
    public void setNavigator(SearchNavigator navigator) {
        super.setNavigator(navigator);
        getmNavigator().setUp();
    }

    public void getVideosBySearch(String searchText) {


        Call<List<Video>> call = getmDataManager().getVideoBySearchText(searchText);
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {

                if(!response.body().isEmpty()){
                    videoBySearchText =response.body();
                    getmNavigator().setVideoListBySearchText();

                }
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {

                getmNavigator().handleError(t.getMessage());
            }
        });



    }

//    public int getSearchVideoListSize() {
//        return this.videoBySearchText.size();
//    }

    public Bitmap setImageBitMap(ImageView imageView, int position) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                if (getVideosList().get(position).getImageUrl() != null) {

                    try {
                        URL url = new URL(getVideosList().get(position).getImageUrl());
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        getNavigator().setImage(image,imageView);
//                        ivVideo.setImageBitmap(image);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

        });
        thread.start();

        return null;
    }

    public List<Video> getVideosList() {
        if(videoBySearchText.size()>0){
            return videoBySearchText;
        }
        return  null;

    }

    public  int getVideoListSize(){
        if(videoBySearchText.size()>0){
            return videoBySearchText.size();
        }
        return  0;

    }
}
