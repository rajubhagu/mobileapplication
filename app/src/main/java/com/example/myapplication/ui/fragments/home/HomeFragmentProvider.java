package com.example.myapplication.ui.fragments.home;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class HomeFragmentProvider {

    @ContributesAndroidInjector
    abstract  HomeFragment provideHomeFragment();
}
