package com.example.myapplication.ui.fragments.home;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.data.network.model.Banner;
import com.example.myapplication.data.network.model.Video;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragmentViewModel extends BaseViewModel<HomeFragmentNavigator> {

    List<String> tags;
    HashMap<String,List<Video>> videoBytagMap;

    List<Video> videoByTag;
    List<Video> videos;

    List<Banner> banners;


   
    public HomeFragmentViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);

    }

    @Override
    public void setNavigator(HomeFragmentNavigator navigator) {
        super.setNavigator(navigator);

        getBanners();
        tags= new ArrayList<>();
        videoBytagMap=new HashMap<>();
        tags.add("song");
        tags.add("horror");
        tags.add("sports");

        for(String tag:tags){
            getVideoByTag(tag);

        }
    }





    private void getVideoByTag(String tag) {

        Call<List<Video>> call = getmDataManager().getVideoByTag(tag);
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {

                if(!response.body().isEmpty()){
                    videoByTag =response.body();
                    videoBytagMap.put(tag,videoByTag);
                    getmNavigator().setVideoListByTag(tag);

                }
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {

                getmNavigator().handleError(t.getMessage());
            }
        });


    }




    private void getBanners() {
        Call<List<Banner>> call = getmDataManager().getBanners();
        call.enqueue(new Callback<List<Banner>>() {
            @Override
            public void onResponse(Call<List<Banner>> call, Response<List<Banner>> response) {

                if(!response.body().isEmpty()){
                    banners=response.body();
                    getmNavigator().setBannerList();
                }
            }

            @Override
            public void onFailure(Call<List<Banner>> call, Throwable t) {

                getmNavigator().handleError(t.getMessage());
            }
        });

    }

    public void getVideos(){
        Call<List<Video>> call = getmDataManager().getVideos();
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {

                if(!response.body().isEmpty()){
                    videos=response.body();
                    getmNavigator().setVideoList();

                }

            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {

                getmNavigator().handleError(t.getMessage());
            }
        });


    }

    public int getVideosSize() {
        if(videos.size()>0){
            return videos.size();
        }
        return 0;

    }

    public List<Video> getVideosList() {
        if(videos.size()>0){
            return videos;
        }
        return null;
    }

    public Bitmap getImage(int position, ImageView ivVideo) {



        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                if (getVideosList().get(position).getImageUrl() != null) {

                    try {
                        URL url = new URL(getVideosList().get(position).getImageUrl());
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                        if(callback != null) {
//                            callback.imageLoaded(image, position);
//                        }
                        getNavigator().setImage(image,ivVideo);
//                        ivVideo.setImageBitmap(image);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

        });
        thread.start();

        return null;

    }

    public  List<Banner> getBannerList(){
        if(banners.size()>0){
            return  banners;
        }
        return  null;
    }


    public int getBannerCount() {

        if(banners.size()>0){
            return banners.size();
        }
        return 0;
    }

    public Bitmap getBannerImage(int position, ImageView ivBannerimage) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                if (!getBannerList().get(position).getVideo().getImageUrl().isEmpty()) {

                    try {
                        URL url = new URL(getBannerList().get(position).getVideo().getImageUrl());
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                        if(callback != null) {
//                            callback.imageLoaded(image, position);
//                        }
                        getNavigator().setImage(image,ivBannerimage);
//                        ivVideo.setImageBitmap(image);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

        });
        thread.start();
        return  null;
    }

    public int getVideoCountByTag(String tag) {
        return  videoBytagMap.get(tag).size();
    }

    public  List<Video> getVideoListByTag(String tag){

        if(videoBytagMap.get(tag).size()>0){
            return  videoBytagMap.get(tag);
        }
        return  null;

    }


    public Bitmap getImageByTag(int position, ImageView ivVideo, String tag) {

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                if (!getVideoListByTag(tag).get(position).getImageUrl().isEmpty()) {

                    try {
                        URL url = new URL(getVideoListByTag(tag).get(position).getImageUrl());
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                        if(callback != null) {
//                            callback.imageLoaded(image, position);
//                        }
                        getNavigator().setImage(image,ivVideo);
//                        ivVideo.setImageBitmap(image);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

        });
        thread.start();
        return  null;

    }
}
