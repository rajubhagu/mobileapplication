package com.example.myapplication.ui.Base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import dagger.android.AndroidInjection;

public abstract class BaseActivity<T extends  ViewDataBinding,V extends  BaseViewModel> extends AppCompatActivity implements BaseFragment.Callback{


    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    private ProgressDialog mProgressDialog;
    private T mViewDataBinding;
    private V mViewModel;


    public abstract V getViewModel();

    public abstract
    @LayoutRes
    int getLayoutId();

    public abstract int getBindingVariable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();
    }

    public void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }


    public void showLoading() {
        hideLoading();
        mProgressDialog= new  ProgressDialog(this);
        mProgressDialog.show();
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public  void  handleError(String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }


}
