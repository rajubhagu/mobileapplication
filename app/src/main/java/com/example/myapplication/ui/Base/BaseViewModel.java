package com.example.myapplication.ui.Base;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.utils.rx.SchedulerProvider;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;


public abstract class BaseViewModel<N> extends ViewModel {

    private final DataManager mDataManager;


    private final SchedulerProvider mSchedulerProvider;

    private CompositeDisposable mCompositeDisposable;

    private WeakReference<N> mNavigator;

    public N getmNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public BaseViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        this.mDataManager=dataManager;
        this.mSchedulerProvider=schedulerProvider;
        mCompositeDisposable=new CompositeDisposable();
    }


    public DataManager getmDataManager() {
        return mDataManager;
    }

    public SchedulerProvider getmSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getmCompositeDisposable() {
        return mCompositeDisposable;
    }

    public void setmCompositeDisposable(CompositeDisposable mCompositeDisposable) {
        this.mCompositeDisposable = mCompositeDisposable;
    }

    public N getNavigator() {
        return mNavigator.get();
    }
}
