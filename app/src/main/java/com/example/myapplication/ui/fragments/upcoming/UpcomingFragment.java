package com.example.myapplication.ui.fragments.upcoming;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.BR;
import com.example.myapplication.R;
import com.example.myapplication.ViewModelProviderFactory;
import com.example.myapplication.databinding.UpcomingActivityBinding;
import com.example.myapplication.ui.Base.BaseFragment;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.ui.fragments.upcoming.adapter.UpComingVideoAdapter;
import javax.inject.Inject;

public class UpcomingFragment extends BaseFragment<UpcomingActivityBinding,UpcomingViewModel> implements UpcomingNavigator{

    @Inject
    ViewModelProviderFactory factory;

    UpcomingViewModel upcomingViewModel;

    UpcomingActivityBinding binding;

    UpComingVideoAdapter upComingVideoAdapter;


    @Override
    public UpcomingViewModel getViewModel() {
        upcomingViewModel= ViewModelProviders.of(this,factory).get(UpcomingViewModel.class);
        return upcomingViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.upcoming_activity;
    }

    @Override
    public int getBindingVariable() {
        return com.example.myapplication.BR.viewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=getViewDataBinding();
        upcomingViewModel.setNavigator(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(binding==null){
            binding=getViewDataBinding();
            upcomingViewModel.setNavigator(this);
        }
        return binding.getRoot();
    }

    @Override
    public void setUpUpcomingVideos() {


    }

    @Override
    public void setImage(Bitmap image, ImageView imageView) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageBitmap(image);
            }
        });

    }


}
