package com.example.myapplication.ui.fragments.search;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.example.myapplication.ui.Base.BaseNavigator;

public interface SearchNavigator extends BaseNavigator {
    void setUp();


    void handleError(String s);
    void setVideoListBySearchText();

    void setImage(Bitmap image, ImageView imageView);
}
