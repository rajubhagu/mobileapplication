package com.example.myapplication.ui.fragments.home;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.myapplication.R;
import com.example.myapplication.ViewModelProviderFactory;
import com.example.myapplication.databinding.HomeActivityBinding;
import com.example.myapplication.ui.Base.BaseFragment;

import com.example.myapplication.ui.fragments.home.adapter.CustomPagersAdapter;
import com.example.myapplication.ui.fragments.home.adapter.VideoListsAdapter;
import com.example.myapplication.ui.fragments.home.adapter.VideoListsByTagAdapter;

import javax.inject.Inject;

public class HomeFragment extends BaseFragment<HomeActivityBinding, HomeFragmentViewModel>  implements  HomeFragmentNavigator  {

    @Inject
    ViewModelProviderFactory factory;

    VideoListsByTagAdapter videoListByTagAdapter;

    VideoListsAdapter adapter;

    HomeFragmentViewModel homeViewModel;

    HomeActivityBinding binding;

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  getViewDataBinding();
        homeViewModel.setNavigator(this);

    }

    @Override
    public HomeFragmentViewModel getViewModel() {
        homeViewModel= ViewModelProviders.of(this).get(HomeFragmentViewModel.class);
        return homeViewModel;
    }


    @Override
    public int getLayoutId()
    {
        return R.layout.home_activity;
    }


    @Override
    public int getBindingVariable() {

        return com.example.myapplication.BR.viewModel;
    }



    @Override
    public void handleError(String s) {
        super.handleError(s);
    }

    @Override
    public void setVideoList() {

        adapter=new VideoListsAdapter(homeViewModel);
        binding.rvSongs.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvSongs.setAdapter(adapter);

    }

    @Override
    public void setImage(Bitmap image, ImageView ivVideo) {
//        ivVideo.setImageBitmap(image);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ivVideo.setImageBitmap(image);
            }
        });
    }


    @Override
    public void setBannerList() {
        binding.viewPager.setAdapter(new CustomPagersAdapter(homeViewModel));

    }

    @Override
    public void setVideoListByTag(String tag) {
        videoListByTagAdapter=new VideoListsByTagAdapter(homeViewModel,tag);
        if(tag.equals("song")){
            binding.rvSongs.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,true));
            binding.rvSongs.setAdapter(videoListByTagAdapter);

        }else if(tag.equals("horror")){
            binding.rvHorror.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,true));
            binding.rvHorror.setAdapter(videoListByTagAdapter);
        }
        else if(tag.equals("sports")){
            binding.rvSports.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,true));
            binding.rvSports.setAdapter(videoListByTagAdapter);
        }

    }





}
