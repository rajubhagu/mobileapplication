package com.example.myapplication.ui.fragments.upcoming.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.databinding.UpcomingVideoItemBinding;
import com.example.myapplication.ui.fragments.upcoming.UpcomingViewModel;

import javax.inject.Inject;

public class UpComingVideoAdapter extends RecyclerView.Adapter<UpComingVideoAdapter.ItemViewHolder> {

    UpcomingViewModel upcomingViewModel;

    @Inject
    public UpComingVideoAdapter(UpcomingViewModel upcomingViewModel) {
        this.upcomingViewModel = upcomingViewModel;
    }

    @NonNull
    @Override
    public UpComingVideoAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UpcomingVideoItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.upcoming_video_item,parent,false);
        return new UpComingVideoAdapter.ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder( UpComingVideoAdapter.ItemViewHolder holder, int position) {
        holder.binding.ivVideo.setImageBitmap(upcomingViewModel.setImageBitMap(holder.binding.ivVideo,position));
        holder.binding.tvTitle.setText(upcomingViewModel.getVideosList().get(position).getTitle());
        holder.binding.tvDesc.setText(upcomingViewModel.getVideosList().get(position).getTitle());
        holder.binding.year.setText(upcomingViewModel.getVideosList().get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return upcomingViewModel.getVideosSize();
    }

    public class ItemViewHolder  extends RecyclerView.ViewHolder {
        UpcomingVideoItemBinding binding;
        public ItemViewHolder( UpcomingVideoItemBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;
        }
    }
}
