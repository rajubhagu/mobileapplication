package com.example.myapplication.ui.fragments.upcoming;

import android.graphics.Bitmap;
import android.widget.ImageView;

public interface UpcomingNavigator {

    void setUpUpcomingVideos();
    void handleError(String s);

    void  setImage(Bitmap image, ImageView imageView);
}
