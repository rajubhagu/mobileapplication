package com.example.myapplication.ui.main;

import android.view.View;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel<MainNavigator> {


    @Inject
    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);

    }

    @Override
    public void setNavigator(MainNavigator navigator) {
        super.setNavigator(navigator);
        getmNavigator().setUp();
    }
}
