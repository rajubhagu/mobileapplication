package com.example.myapplication.ui.login;

import android.text.TextUtils;
import android.view.View;

import com.example.myapplication.data.DataManager;
import com.example.myapplication.data.network.model.Users;
import com.example.myapplication.data.network.model.request.LoginRequest;
import com.example.myapplication.ui.Base.BaseViewModel;
import com.example.myapplication.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {



        List<Users> users;

        String email;

    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public  void login(String email,String password){
        this.email=email;

        Call<List<Users>> call = getmDataManager().getUsers();
        call.enqueue(new Callback<List<Users>>() {
            @Override
            public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {

              users=response.body();

                if((!users.isEmpty())&&users.size()>0){
                    for(Users users1:users){
                        if(email.equals(users1.getEmail())){
//                            getNavigator().openMainActivity();
                            break;
                        }
                        else{
                            getNavigator().handleError("User not found.. SignUp to Register ");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Users>> call, Throwable t) {
                getmNavigator().handleError(t.getMessage());
            }
        });

    }
    public void onLoginClick() {
        getNavigator().login();
    }

    public  void  onSignUpClick(){
        getNavigator().openSignUpActivity();
    }


    public boolean isEmailAndPasswordValid(String email, String password) {

        if (TextUtils.isEmpty(email)) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            return false;
        }
        return true;

    }
}
