package com.example.myapplication.ui.fragments.search.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myapplication.R;
import com.example.myapplication.databinding.SearchVideoItemBinding;
import com.example.myapplication.ui.fragments.search.SearchViewModel;

import javax.inject.Inject;

public class SearchVideoAdapter extends RecyclerView.Adapter <SearchVideoAdapter.ItemViewHolder>{


    SearchViewModel searchViewModel;

    @Inject
    public SearchVideoAdapter(SearchViewModel searchViewModel) {
        this.searchViewModel = searchViewModel;
    }

    @NonNull
    @Override
    public SearchVideoAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SearchVideoItemBinding searchVideoItemBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.search_video_item,parent,false);
        return  new SearchVideoAdapter.ItemViewHolder(searchVideoItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchVideoAdapter.ItemViewHolder holder, int position) {
        holder.searchVideoItemBinding.ivVideo.setImageBitmap(searchViewModel.setImageBitMap(holder.searchVideoItemBinding.ivVideo,position));
        holder.searchVideoItemBinding.title.setText(searchViewModel.getVideosList().get(position).getTitle());
        holder.searchVideoItemBinding.year.setText(searchViewModel.getVideosList().get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return searchViewModel.getVideoListSize();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        SearchVideoItemBinding searchVideoItemBinding;
        public ItemViewHolder(SearchVideoItemBinding binding) {
            super(binding.getRoot());
            this.searchVideoItemBinding=binding;
        }
    }
}
