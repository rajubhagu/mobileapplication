package com.example.myapplication.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;
import com.example.myapplication.ViewModelProviderFactory;
import com.example.myapplication.dagger.builder.ActivityBuilder;
import com.example.myapplication.databinding.LoginActivityBinding;
import com.example.myapplication.ui.Base.BaseActivity;
import com.example.myapplication.ui.signup.SignUpActivity;

import javax.inject.Inject;

public  class LoginActivity extends BaseActivity<LoginActivityBinding, LoginViewModel> implements  LoginNavigator {



    LoginActivityBinding loginActivityBinding;

    @Inject
    ViewModelProviderFactory factory;
    private LoginViewModel mLoginViewModel;




    @Override
    public LoginViewModel getViewModel() {
        mLoginViewModel = ViewModelProviders.of(this,factory).get(LoginViewModel.class);
        return mLoginViewModel;
    }


    @Override
    public int getLayoutId() {
        return R.layout.login_activity;
    }

    @Override
    public int getBindingVariable() {
        return com.example.myapplication.BR.viewModel;

    }

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginActivityBinding = getViewDataBinding();
        mLoginViewModel.setNavigator(this);


    }


    @Override
    public void login() {
        String email=loginActivityBinding.email.getText().toString();
        String password=loginActivityBinding.password.getText().toString();

        if (mLoginViewModel.isEmailAndPasswordValid(email, password)) {

            mLoginViewModel.login(email, password);
        } else {
            Toast.makeText(this, "Invalid Data", Toast.LENGTH_SHORT).show();
        }
        mLoginViewModel.login(email,password);

    }



    @Override
    public void openSignUpActivity() {

        Intent intent = SignUpActivity.newIntent(LoginActivity.this);
        startActivity(intent);
        finish();


    }

    @Override
    public void handleError(String s) {

        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();

    }
}
