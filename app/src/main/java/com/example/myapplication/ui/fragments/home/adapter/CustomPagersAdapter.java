package com.example.myapplication.ui.fragments.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.example.myapplication.R;
import com.example.myapplication.databinding.BannerItemBinding;
import com.example.myapplication.ui.fragments.home.HomeFragmentViewModel;

import javax.inject.Inject;

public class CustomPagersAdapter extends PagerAdapter {

    private HomeFragmentViewModel homeViewModel;


    @Inject
    public CustomPagersAdapter(HomeFragmentViewModel viewModel) {
        this.homeViewModel=viewModel;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        BannerItemBinding bannerItemBinding= DataBindingUtil.inflate(LayoutInflater.from(container.getContext()),R.layout.banner_item,container,false);
        bannerItemBinding.ivBannerimage.setImageBitmap(homeViewModel.getBannerImage(position,bannerItemBinding.ivBannerimage));

        container.addView(bannerItemBinding.getRoot());



        bannerItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBannerClicked(container.getContext(),position);

            }
        });
        return  bannerItemBinding.getRoot();
//        return super.instantiateItem(container, position);

    }

    private void onBannerClicked(Context context, int position) {

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return homeViewModel.getBannerCount();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }
}
