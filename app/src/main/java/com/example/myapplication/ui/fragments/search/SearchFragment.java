package com.example.myapplication.ui.fragments.search;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;
import com.example.myapplication.ViewModelProviderFactory;
import com.example.myapplication.databinding.SearchActivityBinding;
import com.example.myapplication.ui.Base.BaseFragment;
import com.example.myapplication.ui.fragments.search.adapter.SearchVideoAdapter;

import javax.inject.Inject;

public class SearchFragment extends BaseFragment<SearchActivityBinding,SearchViewModel> implements SearchNavigator {

    @Inject
    ViewModelProviderFactory factory;

    SearchViewModel searchViewModel;

    SearchVideoAdapter searchVideoAdapter;

    SearchActivityBinding binding;


    @Override
    public SearchViewModel getViewModel() {
        searchViewModel= ViewModelProviders.of(this,factory).get(SearchViewModel.class);
        return searchViewModel;
    }

    @Override
    public int getLayoutId() {

        return R.layout.search_activity;
    }

    @Override
    public int getBindingVariable() {
        return com.example.myapplication.BR.viewModel;
    }


    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        searchViewModel.setNavigator(this);
    }

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        if (binding == null) {
            binding = getViewDataBinding();
            searchViewModel.setNavigator(this);
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated( View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void setUp() {
        binding.search.setOnSearchClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText=binding.search.toString();
                searchViewModel.getVideosBySearch(searchText);
            }
        });

    }

    @Override
    public void setVideoListBySearchText() {

    }

    @Override
    public void setImage(Bitmap image, ImageView imageView) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageBitmap(image);
            }
        });

    }

    @Override
    public void handleError(String s) {
        super.handleError(s);
    }
}
