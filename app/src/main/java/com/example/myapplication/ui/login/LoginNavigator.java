package com.example.myapplication.ui.login;

public interface LoginNavigator {
    void login();


    void openSignUpActivity();

    void handleError(String s);
}
