/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.example.myapplication.data.network;



import com.example.myapplication.data.network.model.Banner;
import com.example.myapplication.data.network.model.Users;
import com.example.myapplication.data.network.model.Video;

import java.util.List;

import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Singleton
public class AppApiHelper implements ApiHelper {

    ApiHelper apiHelper;
    public AppApiHelper() {
        apiHelper=retrofit.create(ApiHelper.class);
    }

    Retrofit retrofit=new Retrofit.Builder()
            .baseUrl("http://192.168.4.236:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    public Call<List<Video>> getVideos() {
        return apiHelper.getVideos();
    }

    @Override
    public Call<List<Users>> getUsers() {
        return apiHelper.getUsers();
    }

    @Override
    public Call<List<Banner>> getBanners() {
        return apiHelper.getBanners();
    }

    @Override
    public Call<List<Video>> getVideoByTag(String tag) {
        return apiHelper.getVideoByTag(tag);
    }

    @Override
    public Call<List<Video>> getVideoBySearchText(String searchText) {
        return apiHelper.getVideoBySearchText(searchText);
    }

    @Override
    public Call<List<Video>> getUpcomingVideos() {
        return apiHelper.getUpcomingVideos();
    }
}
