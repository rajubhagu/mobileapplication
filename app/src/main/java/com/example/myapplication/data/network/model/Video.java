package com.example.myapplication.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Video {


    @Expose
     @SerializedName("videoUrl")
    String videoUrl;

    @Expose
    @SerializedName("imageUrl")
    String imageUrl;

    @Expose
    @SerializedName("title")
    String title;


    @Expose
    @SerializedName("date")
    String date;



    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImageUrl() {
//        try {
//            URL url = new URL(imageUrl);
//            return url;
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
