

package com.example.myapplication.data;

import android.content.Context;

import com.example.myapplication.data.network.ApiHelper;
import com.example.myapplication.data.network.model.Banner;
import com.example.myapplication.data.network.model.Users;
import com.example.myapplication.data.network.model.Video;
import com.example.myapplication.data.network.model.request.LoginRequest;
import com.example.myapplication.data.network.model.response.LoginResponse;
import com.example.myapplication.data.pref.PreferenceHelper;
import com.google.gson.Gson;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;


/**
 * Created by amitshekhar on 07/07/17.
 */
@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final ApiHelper mApiHelper;

    private final Context mContext;


    private final Gson mGson;

    private final PreferenceHelper mPreferencesHelper;

    @Inject
    public AppDataManager(Context context, PreferenceHelper preferencesHelper, ApiHelper apiHelper, Gson gson) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mGson = gson;
    }


    @Override
    public Call<List<Video>> getVideos() {
        return mApiHelper.getVideos();
    }


    @Override
    public Call<List<Users>> getUsers() {
        return mApiHelper.getUsers();
    }

    @Override
    public Call<List<Banner>> getBanners() {
        return mApiHelper.getBanners();
    }

    @Override
    public Call<List<Video>> getVideoByTag(String tag) {
        return mApiHelper.getVideoByTag(tag);
    }

    @Override
    public Call<List<Video>> getVideoBySearchText(String searchText) {
        return mApiHelper.getVideoBySearchText(searchText);
    }

    @Override
    public Call<List<Video>> getUpcomingVideos() {
        return mApiHelper.getUpcomingVideos();
    }
}
