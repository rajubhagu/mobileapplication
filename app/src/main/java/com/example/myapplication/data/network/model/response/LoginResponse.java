package com.example.myapplication.data.network.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {


    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("is_user_present")
    private String isUserPresent ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsUserPresent() {
        return isUserPresent;
    }

    public void setIsUserPresent(String isUserPresent) {
        this.isUserPresent = isUserPresent;
    }
}
