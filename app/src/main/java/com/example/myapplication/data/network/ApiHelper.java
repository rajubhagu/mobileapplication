/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.example.myapplication.data.network;


import com.example.myapplication.data.network.model.Banner;
import com.example.myapplication.data.network.model.Users;
import com.example.myapplication.data.network.model.Video;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiHelper {

    @GET("/getVideos")
    Call<List<Video>> getVideos();


    @GET("getUsers")
    Call<List<Users>> getUsers();


    @GET("home/getBanners")
    Call<List<Banner>> getBanners();


    @GET("home/getVideoByTag")
    Call<List<Video>> getVideoByTag(@Query("tag") String tag);


    @GET("/search/")
    Call<List<Video>> getVideoBySearchText(@Query("tag") String searchText);


    @GET("/")
    Call<List<Video>> getUpcomingVideos();
}
