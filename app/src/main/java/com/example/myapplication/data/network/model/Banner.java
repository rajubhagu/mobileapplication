package com.example.myapplication.data.network.model;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banner implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("video")
    @Expose
    private Video video;
    private final static long serialVersionUID = -1031482305088211210L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }




    public class Video implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("videoUrl")
        @Expose
        private String videoUrl;
        @SerializedName("tag")
        @Expose
        private Object tag;
        @SerializedName("imageUrl")
        @Expose
        private String imageUrl;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("languages")
        @Expose
        private Object languages;
        @SerializedName("description")
        @Expose
        private Object description;
        @SerializedName("date")
        @Expose
        private Object date;
        private final static long serialVersionUID = 7975469982662129731L;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public Object getTag() {
            return tag;
        }

        public void setTag(Object tag) {
            this.tag = tag;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Object getLanguages() {
            return languages;
        }

        public void setLanguages(Object languages) {
            this.languages = languages;
        }

        public Object getDescription() {
            return description;
        }

        public void setDescription(Object description) {
            this.description = description;
        }

        public Object getDate() {
            return date;
        }

        public void setDate(Object date) {
            this.date = date;
        }
    }
}