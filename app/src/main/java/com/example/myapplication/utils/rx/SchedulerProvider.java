package com.example.myapplication.utils.rx;

import io.reactivex.Scheduler;

/**
 * Created by Arshdeep Singh on 11/17/17.
 */

public interface SchedulerProvider {

    Scheduler ui();
    Scheduler computational();
    Scheduler io();

}
